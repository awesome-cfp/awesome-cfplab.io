# Awesome CFP Tooling

> A curated list of tools for CFP and speaking event management.

- [For Speakers](#speakers)
  - [Finding Events](#finding-events)
  - [Managing Abstracts & Submissions](#managing-abstracts--submissions)
  - [Publishing](#publishing)
- [For Event Hosts](#event-hosts)
  - [Managing a Call for Papers](#managing-a-call-for-papers)
  - [Finding Speakers](#finding-speakers)
  - [Event Management](#event-managment)
  - [Code of Conduct Resources](#code-of-conduct-resources)

## Speakers

### Finding Events

- [CFPland](https://www.cfpland.com/) Sign up to get upcoming Calls for Proposals in your inbox every week.
- [SeeCFP](https://seecfp.com/) Never miss a CFP again!
- [DevRel Collective](https://devrelcollective.fun/) A place for DevRel professionals, Community Managers, and others to share resources, learn best practices, support one another, and be amongst our peers
- [GetTogether.community](https://gettogether.community/) Find nearby events
- [Developers.events](https://developers.events/) This repository lists a maximum of conference dates to help conference organizers.
- [Meetups.com](https://www.meetup.com/) Find local or remote meetups to speak at
- [Confs.tech](https://confs.tech/#) List of all tech conferences in 2022 and 2023Find your next tech conference
- [CFP tool](https://github.com/lju-lazarevic/cfptool) Making life easier for finding relevant conference call for papers

### Managing Abstracts & Submissions

- [CFPs.dev](https://cfps.dev) Open source abstract and submission management platform
- [Xela](https://github.com/mattstratton/xela) A webapp for tracking sponsorship, speaking, and cfps for events.

Also, some platforms that are made for [managing a CFP](#managing-a-call-for-papers) also offer the ability for Speakers to save abstracts and do some other management.

### Publishing

- [GitHub Pages]()
- [GitLab Page]()

## Event Hosts

### Managing a Call for Papers

- [Sessionize](https://sessionize.com/) Call for Papers, Schedule and Speaker Management.
- [PaperCall](https://www.papercall.io/) aperCall enables event organizers to easily manage their call for papers and talk submissions
- [Pretalx](https://github.com/pretalx/pretalx) Conference planning tool: CfP, scheduling, speaker management
- [Xela](https://github.com/mattstratton/xela) A webapp for tracking sponsorship, speaking, and cfps for events.

### Finding Speakers

- [Speaker Hub](https://speakerhub.com/) Find the perfect speaker: For events, training workshops or schools - no commission fee

### Event Managment

### Code of Conduct Resources

- [Conf Code of Conduct](https://confcodeofconduct.com/) A ready to go conference code of conduct

#### Examples

- [JSConf EU 2019](https://2019.jsconf.eu/code-of-conduct/)
- [Rust Community](https://www.rust-lang.org/policies/code-of-conduct)
